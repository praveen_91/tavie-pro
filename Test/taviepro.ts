import { browser, element, by } from "protractor";
import { LoginPage } from "../pages/LoginPage";
import { CommonPage } from "../pages/CommonPage";
import { MyAccountPage } from "../pages/MyAccountPage";
var common = new CommonPage();
describe("Login", () => {
    var login = new LoginPage();
    
    it("Verify LoginPage", () => {
        login.OpenBrowser()
        common.ExplicitWait(3000)

        login.VerifyLogin()
        common.ExplicitWait(3000)
        console.log("Login Successful")
        login.VerifyLandingPage()
        console.log("Navigated to My Account page")
    })
})

describe("MyAccount",()=>{
         var myaccount = new MyAccountPage()
    it("UpdateAccountDetails",()=>{
        common.ExplicitWait(3000)
         myaccount.UpdateMyAccountDetails()
         
    })

})