import {browser}from "protractor"
var jasmine2HtmlReporter = require('protractor-jasmine2-html-reporter');
exports.config = {
    directConnect: true,
  
    
  
    // Framework to use. Jasmine is recommended.
    framework: 'jasmine',
    jasmineNodeOpts: {
        defaultTimeoutInterval: 2500000
      },
      onPrepare: function () {
        browser.manage().window().maximize()

        jasmine.getEnv().addReporter(new jasmine2HtmlReporter({
          savePath: './Reports',
          fixedScreenshotName: true,
          fileNamePrefix : 'TAVIEPRO_REPORT'
        }))
      },
    // Spec patterns are relative to the current working directory when
    // protractor is called.
    seleniumAddress: 'http://localhost:4444/wd/hub',
    specs: ['taviepro.js'],

    capabilities: {
        'browserName': 'chrome'
      }
  
    
    
  };
  